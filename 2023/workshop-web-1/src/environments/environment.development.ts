export const environment = {
    production: false,
  
    // Service URL
    usersEndpoint: "https://jsonplaceholder.typicode.com",
    lambdaUserEndpoint: "https://cyfnxeyaj7ay5vfcowma4m73he0sxlmz.lambda-url.us-east-1.on.aws"
  };
  