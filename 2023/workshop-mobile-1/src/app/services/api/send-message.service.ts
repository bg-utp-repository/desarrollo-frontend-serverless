import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SendMessageService {

  constructor(private http: HttpClient) { }

    sendSMS(TypeOfM: any, phoneNumber: any,message: any): void {
      
      const body = {
        phoneNumber: '+507'+phoneNumber,
        message: message
      };
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    
      this.http.post(environment.lambdaMessageService, body, { headers })
        .pipe(
          catchError(error => {
            console.error('Ocurrió un error al realizar la solicitud POST', error);
            return of(error);
          })
        )
        .subscribe(response => {
          console.log('La solicitud POST se completó con éxito', response);
        });
    }

  };

  