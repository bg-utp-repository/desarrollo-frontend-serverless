import { Component } from '@angular/core';
import { SendMessageService } from '../services/api/send-message.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  Message?: String;
  Phone?: String;
  
  constructor(private sendMsg: SendMessageService) { }

  sendSMS(): void{
    console.log(this.Message, this.Phone);
    
    this.sendMsg.sendSMS('SMS', this.Message, this.Phone)
  }
    
}
