export const environment = {
  production: true,

  // Service URL
  usersEndpoint: "https://jsonplaceholder.typicode.com"
};
