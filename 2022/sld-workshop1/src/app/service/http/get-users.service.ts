import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/shared/models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetUsersService {

  private usersEndpoint = environment.usersEndpoint;

  constructor(private http: HttpClient) {}

  getUsers(): Observable <User[]> {
    return this.http.get(`${this.usersEndpoint}/users`).pipe(
      map((r: User[]) => {
       // Operaciones generales sobre los datos
        console.log(`Desde el Servicio   | longitud de data: ${r.length}`);

        return r;
      })
    );
  }

}
