# Desarrollo Frontend Serverless :zap:

## Documentación general :point_down:
- Dentro de la carpeta `ppt` está el material teórico
- Dentro de la carpeta `sld-workshop1` está el código y la documentación de la primera práctica
- Dentro de la carpeta `sld-workshop2` está el código y la documentación de la segunda práctica

### Práctica #3 Funciones Lambda en IBM Cloud

> Documentación: https://cloud.ibm.com/apidocs/functions

1. En la barra de busqueda, escribir `Functions`
2. Seleccionar la opción de `Create Action`, colócale un nombre como `function-1` y click en `Create`

3. Nuevamente seleccionar la opción de `Create Action`, colócale un nombre como `function-2` y click en `Create`

4. Ahora vamos a crear una secuencia de funciones. Para esto, seleccionar `Create Sequence`, le pones un nombre como `Sequence-f1-f2`. Luego click en `Create Package` para que agrupe nuestras funciones.

5. Estando en esa pantalla, donde dice `select action`, seleccionar la `function-1` previamente creada.

6. Luego, click en la opción de `Add` y agregar la `function-2` y click en `Save`.

7. Estamos listos para invocar nuestras funciones Lambda por separadas y en cadena.


### Links y referencias 
- [Info Serverless](https://blog.neap.co/the-serverless-series-what-is-serverless-d651fbacf3f4)
- [Link de IBM Cloud](https://cloud.ibm.com/)
- [Link de codigo snippet para AWS Lambda con DynamoDB](https://github.com/serverless/examples/tree/master/aws-node-rest-api-with-dynamodb)
- [Link de Serverless Framework](https://app.serverless.com/)
- [Link de referencia para deploy automatico Github con CircleCI](https://github.com/carlosmayorga/angular-pipeline/blob/master/.circleci/config.yml)
